<?php
require_once "logincheck.php";
$curr_room = 'lobby';
$curr_session = "Lobby";
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/lobby.jpg">
            <a href="auditorium.php" id="enterAudi">
                <div class="indicator d-6"></div>
            </a>
            <!--  <a href="https://www.siemens-healthineers.com/en-in/laboratory-automation/systems/aptio-automation" class="resdl" data-docid="1" target="_blank" id="aptio">
                <div class="indicator d-6"></div>
            </a>

            <a href="https://www.siemens-healthineers.com/en-in/diagnostics-it/atellica-diagnostics-it/atellica-data-manager" class="resdl" data-docid="2" target="_blank" id="atellica">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="assets/resources/agenda.jpg" class="view resdl" data-docid="3" id="showAgenda">
                <div class="indicator d-4"></div>
            </a>


        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php";            ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<audio id="lobbyMusic">
    <source src="assets/resources/lobby-music.mp3" type="audio/mpeg">
</audio>
<?php require_once "scripts.php" ?>
<script>
    var lobbyAudio = document.getElementById("lobbyMusic");

    $(function() {
        lobbyAudio.currentTime = 0;
        lobbyAudio.play();
    });
</script>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>