<nav class="navbar bottom-nav">
  <ul class="nav me-auto ms-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="auditorium.php" title="Go To Auditorium"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Auditorium</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>